import { createAppContainer } from 'react-navigation';
import { createMaterialTopTabNavigator } from 'react-navigation-tabs';
import React from 'react';
import GForce from '../screens/GForce';
import StatsScreen from '../screens/StatsScreen';
import PastRuns from '../screens/PastRuns';
import { NAVIGATION_ROUTES } from './constants';
import IconEntypo from 'react-native-vector-icons/Entypo';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Foundation from 'react-native-vector-icons/Foundation';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import * as basicStyle from '../basic/styles';
import SettingsScreen from '../screens/Settings';

const AppNavigator = createMaterialTopTabNavigator(
  {
    [NAVIGATION_ROUTES.STATS.route]: {
      screen: StatsScreen,
      navigationOptions: {
        title: NAVIGATION_ROUTES.STATS.displayName,
        tabBarIcon: ({ tintColor }) =>
          <IconEntypo name="stopwatch" size={25} color={tintColor}/>
      },
    },
    [NAVIGATION_ROUTES.GFORCE.route]: {
      screen: GForce,
      navigationOptions: {
        title: NAVIGATION_ROUTES.GFORCE.displayName,
        tabBarIcon: ({ tintColor }) =>
          <Foundation name="target-two" size={27} color={tintColor}/>
      },
    },
    [NAVIGATION_ROUTES.PAST_RUNS.route]: {
      screen: PastRuns,
      navigationOptions: {
        title: NAVIGATION_ROUTES.PAST_RUNS.displayName,
        tabBarIcon: ({ tintColor }) =>
          <MaterialCommunityIcons name="history" size={27} color={tintColor}/>
      },
    },
    [NAVIGATION_ROUTES.SETTINGS.route]: {
      screen: SettingsScreen,
      navigationOptions: {
        title: NAVIGATION_ROUTES.SETTINGS.displayName,
        tabBarIcon: ({ tintColor }) =>
          <MaterialIcons name="settings" size={27} color={tintColor}/>
      },
    },
  },
  {
    initialRouteName: NAVIGATION_ROUTES.STATS.route,
    tabBarPosition: 'bottom',
    swipeEnabled: false,
    tabBarOptions: {
      showIcon: true,
      showLabel: false,
      allowFontScaling: false,
      activeTintColor: basicStyle.TEXT_LIGHT,
      inactiveTintColor: basicStyle.TEXT_DARK,
      iconStyle: {
        height: 30,
        width: 30,
      },
      indicatorStyle: {
        backgroundColor: basicStyle.BACKGROUND_DARK,
      },
      style: {
        backgroundColor: basicStyle.BACKGROUND_DARK,
      }
    },
  }
);

export default createAppContainer(AppNavigator);
