import React from 'react';
import { View, StyleSheet, FlatList } from 'react-native';
import { NavigationTabScreenProps } from 'react-navigation-tabs';
import * as basic from '../basic';
import * as storage from '../../services/storage';
import { pastRunsConnect, PastRunsReduxProps } from '../../redux/connect/pastRunsConnect';
import PastRunView from './pastRuns/PastRun';

interface PastRunsProps extends PastRunsReduxProps, NavigationTabScreenProps {
}

class PastRunsScreen extends React.Component<PastRunsProps> {

  resetPastRuns = async () => {
    await storage.storeData(storage.PAST_RUNS, {});
  }

  items = (): storage.PastRunsProps[] => {
    return this.props.pastRuns && Object.keys(this.props.pastRuns).map((key) => this.props.pastRuns[key]);
  }

  keyExtractor = (item: storage.PastRunsProps) => `${item.time}:${item.type}`

  render() {
    return (
      <basic.Screen>
        <View style={styles.container}>
          <View style={styles.header}>
            <basic.CustomText size={24} label={'Past runs'} />
          </View>
          <FlatList
            data={this.items()}
            keyExtractor={this.keyExtractor}
            renderItem={({item, index}) => <PastRunView key={index} pastRun={item}/>}
          />
          {/* <View style={styles.body}>
            <basic.CustomText label={`Here you will see your past runs detailed. You'll see the acceleration graph and even the aproximated horse power of you car.`} />
          </View>
          <View style={styles.body}>
            <basic.CustomText label={'Coming soon...'} color={'yellow'} />
          </View> */}
        </View>
      </basic.Screen>
    );
  }
}

export default pastRunsConnect(PastRunsScreen);

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  header: {
    paddingLeft: 20,
    paddingTop: 10,
  },
  body: {
    paddingLeft: 20,
    paddingTop: 10,
  }
});
