import React from 'react';
import { View, StyleSheet, Switch } from 'react-native';
import { NavigationTabScreenProps } from 'react-navigation-tabs';
// import { AdMobBanner } from 'expo-ads-admob';
import * as basic from '../basic';
import * as storage from '../../services/storage';
import * as basicStyles from '../basic/styles';
import { carConnect, CarReduxProps } from '../../redux/connect/carConnect';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';

interface SettingsScreenProps extends CarReduxProps, NavigationTabScreenProps {
}

class SettingsScreen extends React.Component<SettingsScreenProps> {

  changeMetric = async (value: boolean) => {
    await this.storeIsMetric(value)
    this.props.setIsMetric(value);
  }

  storeIsMetric = async (value: boolean) => {
    await storage.storeData(storage.IS_METRIC_KEY, value);
  }

  resetStore =  async () => {
    this.props.setCars(storage.defaultCar);
    await storage.storeData(storage.CARS_IDS, storage.defaultCar);
  }

  resetPastRuns =  async () => {
    await storage.storeData(storage.PAST_RUNS, {});
  }

  bannerError = () => {
  }

  render() {
    return (
      <basic.Screen>
        <View style={styles.header}>
          <View style={styles.card}>
            <View style={styles.cardIcon}>
              <MaterialIcons name="settings" size={250} color={basicStyles.PALE_BLUE} />
            </View>
            <basic.CustomText color={'light'} label={'Settings'} size={40}/>
          </View>
          <View style={styles.metricContainer}>
            <View style={styles.inline}>
              <basic.CustomText color={'dark'} size={24}
                    label={`Unit ${this.props.isMetric ? 'Metric' : 'Imperial'}`} />
              <View style={styles.switch}>
                <Switch value={this.props.isMetric} onValueChange={this.changeMetric}
                  trackColor={{
                    false: basicStyles.TEXT_DARK,
                    true: basicStyles.ACCENT_YELLOW
                  }}
                  />
              </View>
            </View>
            <View style={styles.resetStats}>
              <View style={styles.resetStatsText}>
                <basic.CustomText color={'dark'} label={'Reset all stats'} size={24} />
              </View>
              <basic.PrimaryButton onPress={this.resetStore} size={30}/>
            </View>
            <View style={styles.resetStats}>
              <View style={styles.resetStatsText}>
                <basic.CustomText color={'dark'} label={'Reset past runs'} size={24} />
              </View>
              <basic.PrimaryButton onPress={this.resetPastRuns} size={30}/>
            </View>
            <View style={styles.description}>
              <basic.CustomText color={'dark'} size={14} label={`More settings comming soon, if you have any trouble using the app or there are some features that you want, let us know and we will come with an update soon.`} />
            </View>
          </View>
        </View>
        <View style={styles.footer}>
        {/* <AdMobBanner
          bannerSize="fullBanner"
          adUnitID={Platform.OS === 'ios' ?
            'ca-app-pub-5724098793803537/8631776982': 'ca-app-pub-5724098793803537/7668187289'}
          servePersonalizedAds
          onDidFailToReceiveAdWithError={this.bannerError} /> */}
        </View>
      </basic.Screen>
    );
  }
}

export default carConnect(SettingsScreen);

const styles = StyleSheet.create({
  header: {
    flex: 3,
    alignItems: 'center',
    marginTop: 20,
    padding: 20,
  },
  card: {
    flex: 1,
    width: '100%',
    borderRadius: 10,
    backgroundColor: basicStyles.BACKGROUND_LIGHT,
    shadowColor: "#131417",
    shadowOffset: {
      width: 2,
      height: 1,
    },
    shadowOpacity: 1,
    shadowRadius: 4.00,
    elevation: 8,
    justifyContent: 'center',
    alignItems: 'center',
    overflow: 'hidden',
  },
  cardIcon: {
    position: 'absolute',
    left: 0,
    top: -20,
  },
  metricContainer: {
    flex: 4,
    width: '100%',
    marginTop: 20,
    alignItems: 'center',
  },
  inline: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    width: '100%',
  },
  resetStats: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    width: '100%',
    marginTop: 20,
    marginLeft: 20,
    marginRight: 20,
  },
  resetStatsText: {
    marginRight: 15,
  },
  description: {
    marginTop: 50,
  },
  footer: {
    flex: 1,
    justifyContent: 'flex-end',
  },
  switch: {
    marginLeft: 20,
    flexDirection: 'row',
  },
});
