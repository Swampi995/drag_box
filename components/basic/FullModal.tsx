import React, { FunctionComponent } from 'react';
import { StyleSheet, View } from 'react-native';
import * as basicStyles from './styles';
import Modal from 'react-native-modal';

interface FullModalProps extends React.Props<{}> {
  visible: boolean;
}

export const FullModal: FunctionComponent<FullModalProps> = ({ visible, children }) => {
  return (
    <Modal isVisible={visible} hasBackdrop={false}>
      <View style={styles.centeredView}>
        <View style={styles.modalView}>
          {children}
        </View>
      </View>
    </Modal>
  );
}

const styles = StyleSheet.create({
  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  modalView: {
    padding: 20,
    backgroundColor: basicStyles.BACKGROUND_LIGHT,
    width: '115%',
    height: '115%',
  },
});

export default FullModal;
