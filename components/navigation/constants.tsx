
export const STATS_ROUTE = 'stats';
export const GFORCE_ROUTE = 'gforce';
export const PAST_RUNS = 'past-runs';
export const SETTINGS_ROUTE = 'settings';

export const NAVIGATION_ROUTES = {
  STATS: {
    route: STATS_ROUTE,
    displayName: 'Time Stats',
  },
  GFORCE: {
    route: GFORCE_ROUTE,
    displayName: 'G Force',
  },
  PAST_RUNS: {
    route: PAST_RUNS,
    displayName: 'Past Runs',
  },
  SETTINGS: {
    route: SETTINGS_ROUTE,
    displayName: 'Settings',
  },
}
