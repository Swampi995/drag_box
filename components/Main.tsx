import React from 'react';
import { activateKeepAwake } from 'expo-keep-awake';
import * as Updates from 'expo-updates';
import * as Sentry from 'sentry-expo';

Sentry.init({
 dsn: 'https://39832f2e32b74cc29e71f2fc1bbd5f92@o372933.ingest.sentry.io/5371800',
 enableInExpoDevelopment: false,
 debug: true,
});

import { carConnect, CarReduxProps } from '../redux/connect/carConnect';
import { pastRunsConnect, PastRunsReduxProps } from '../redux/connect/pastRunsConnect';
import * as storage from '../services/storage';
import * as service from '../services/storage/service';

interface MainProps extends CarReduxProps, PastRunsReduxProps {
}

class Main extends React.Component<MainProps> {

  async componentDidMount() {
     const cars = await storage.retrieveData<storage.CarsObject>(storage.CARS_IDS);
     const selectedKey = await storage.retrieveData<string>(storage.SELECTED_KEY);
     const isMetric = await storage.retrieveData<boolean>(storage.IS_METRIC_KEY);

     if (!cars) {
       await storage.storeData(storage.CARS_IDS, storage.defaultCar);
       await storage.storeData(storage.SELECTED_KEY, 'default');
     }

     this.props.setCars(cars || storage.defaultCar);
     this.props.setSelectedCar(selectedKey || 'default');
     this.props.setIsMetric(isMetric === undefined ? true : isMetric);

     const pastRuns = await service.getPastRuns();
     this.props.setPastRuns(pastRuns);

     this.activateKeepAwake();

     if (!__DEV__) {
       const update = await Updates.fetchUpdateAsync();
       if (update && update.isNew) {
         await Updates.reloadAsync();
       }
     }
  }

  activateKeepAwake = () => {
    activateKeepAwake();
  }

  render() {
    return null;
  }
}

export default pastRunsConnect(carConnect(Main));
